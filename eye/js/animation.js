'use strict';

// document.querySelector('.big-book__pupil').style.setProperty('--pupil-y', '30px');
//clientWidth/Height для элемента document.documentElement – это как раз ширина/высота видимой области окна.

const eyePosititon = document.querySelector('.big-book__pupil').getBoundingClientRect();
const eyeNode = document.querySelector('.big-book__pupil');
const fromEye = {
  get top() {
    return eyePosititon.top + eyePosititon.height / 2
  },
  get bottom() {
    return document.documentElement.clientHeight - eyePosititon.bottom + eyePosititon.height / 2
  },
  get left() {
    return eyePosititon.left + eyePosititon.width / 2
  },
  get right() {
    return document.documentElement.clientWidth - eyePosititon.right + eyePosititon.width / 2
  },
  x: 0,
  y: 0,
  sizePupil: 3,
  sizeEye: 60
}

document.addEventListener('mousemove', mainFunction);

function mainFunction(event) {
  if (event.clientY < eyePosititon.top) {
    eyeNode.style.setProperty('--pupil-y', `${event.clientY / fromEye.top * fromEye.sizeEye / 2 - 30}px`);
    fromEye.y = event.clientY / fromEye.top;
  } else {
    eyeNode.style.setProperty('--pupil-y', `${(event.clientY - fromEye.top) / fromEye.bottom * fromEye.sizeEye / 2}px`);
    fromEye.y = 1 - (event.clientY - fromEye.top) / fromEye.bottom;
  }

  if (event.clientX < eyePosititon.left) {
    eyeNode.style.setProperty('--pupil-x', `${event.clientX / fromEye.left * fromEye.sizeEye / 2 - 30}px`);
    fromEye.x = event.clientX / fromEye.left;
  } else {
    eyeNode.style.setProperty('--pupil-x', `${(event.clientX - fromEye.left) / fromEye.right * fromEye.sizeEye / 2}px`);
    fromEye.x = 1 - (event.clientX - fromEye.left) / fromEye.right;
  }

  eyeNode.style.setProperty('--pupil-size', Math.min(fromEye.x, fromEye.y) * fromEye.sizePupil);
}