'use strict';

document.addEventListener('DOMContentLoaded', () => {

  const errorMessage = document.querySelector('#error-message');
  const shotPictureBtn = document.querySelector('#take-photo');
  const pictureList = document.querySelector('.list');

  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');

  //DOM elements
  const el = (name, props, ...childs) => ({
    name,
    props,
    childs
  });
  const video = el('video', {
    'autoplay': '',
    'class': 'video'
  });
  const picture = el('figure', null,
    el('img', {
      'src': canvas.toDataURL()
    }),
    el(
      'figcaption',
      null,
      el(
        'a', {
          'href': canvas.toDataURL(),
          'download': "snapshot.png"
        },
        el('i', {
          'class': 'material-icons'
        }, 'file_download')),
      el(
        'a',
        null,
        el('i', {
          'class': 'material-icons'
        }, 'file_upload')),
      el(
        'a',
        null,
        el('i', {
          'class': 'material-icons'
        }, 'delete'))
    )
  );

  //events
  document.querySelector('.app').addEventListener('mouseenter', () => {
    document.querySelector('.controls').style.display = 'block'
  });
  document.querySelector('.app').addEventListener('mouseleave', () => {
    document.querySelector('.controls').style.display = ''
  });

  shotPictureBtn.addEventListener('click', () => {
    shotPictureBtn.appendChild(createElement(el('audio', {
      'src': './audio/click.mp3'
    })))
    shotPictureBtn.getElementsByTagName('audio')[0].play();
    createPicture();

  })

  //camera access
  navigator.mediaDevices
    .getUserMedia({
      video: true,
      audio: false
    })
    .then((stream) => {
      document.querySelector('.app').appendChild(createElement(video));
      document.querySelector('.video').srcObject = stream;
    })
    .catch(err => {
      errorMessage.style.display = 'block';
      errorMessage.innerText = err;
    });

  function createElement(node) {

    if ((typeof node === 'string')) {
      return document.createTextNode(node.toString());
    }

    if (Array.isArray(node)) {
      return node.reduce((f, elem) => {
        f.appendChild(createElement(elem));
        return f;
      }, document.createDocumentFragment());
    }

    const element = document.createElement(node.name);

    if (node.props) {
      Object.keys(node.props).forEach(
        key => element.setAttribute(key, node.props[key])
      );
    }

    if (node.childs) {
      element.appendChild(createElement(node.childs))
    };

    return element;
  }

  function createPicture() {
    const videoFrame = document.querySelector('video');
    // проверяем его размеры
    canvas.width = videoFrame.videoHeight;
    canvas.height = videoFrame.videoWidth;

    // копируем текущий кадр видеотега в canvas
    ctx.drawImage(videoFrame, 0, 0);

    picture.childs[1].childs["0"].props.href = canvas.toDataURL();
    picture.childs["0"].props.src = canvas.toDataURL();

    // обновляем картинку на странице
    if (pictureList.firstChild) {
      pictureList.insertBefore(createElement(picture), pictureList.firstChild)
    } else {
      pictureList.appendChild(createElement(picture));
    }

    const pictures = document.querySelectorAll('figcaption');
    Array.from(pictures).forEach(item => {
      item.addEventListener('click', modifyPicture);
    })
  }

  function modifyPicture(event) {

    if (event.target.parentNode === event.currentTarget.children[1]) {
      // uploadBtn = false;
      event.currentTarget.children[1].setAttribute('href', '');
      event.currentTarget.children[1].style.opacity = 0;
      event.preventDefault();
      const pictureData = new FormData()
      canvas.toBlob(blob => {
        pictureData.append('image', blob);
        fetch('https://neto-api.herokuapp.com/photo-booth', {
            method: 'post',
            body: pictureData
          })
          .then(console.log);
      })
    }

    if (event.target.parentNode === event.currentTarget.children[0]) {
      event.currentTarget.children[0].setAttribute('href', '');
      event.currentTarget.children[0].style.opacity = 0;
    }

    if (event.target.parentNode === event.currentTarget.children[2]) {
      event.currentTarget.parentNode.parentNode.removeChild(event.currentTarget.parentNode);
    }

  }
})